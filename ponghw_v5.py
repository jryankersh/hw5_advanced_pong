#Pong Game Paddles Prototype

import sys
from Tkinter import *
import numpy as np
from controllerv5 import *

DELAY =  10

class Board(Canvas):

    def __init__(self, parent, Frame):
        field = Canvas.__init__(self, width=600, height=300,
                                    background="black", highlightthickness=0)
        
        self.Frame = Frame      # this is the GUI Frame passed back from protopong
        self.parent = parent
        self.initGame()
        self.pack()

    def initUI(self):

        #This function defines the score labels.
        label_p1 = Label(self.Frame, text = "  Player 1                               ")
        label_p1.grid(row = 0, column = 0, columnspan=2)

        label_p2 = Label(self.Frame, text = "                               Player 2  ")
        label_p2.grid(row = 0, column = 8, columnspan=2)

        label_score = Label(self.Frame, text = "  Score  ")
        label_score.grid(row = 0, column = 3) 
        
        self.player1_points = IntVar()
        self.label_p1_score_value = Label(self.Frame,textvariable=self.player1_points)
        self.label_p1_score_value.grid(row = 0, column = 2)
        
        self.player2_points = IntVar()
        self.label_p2_score_value = Label(self.Frame,textvariable = self.player2_points)
        self.label_p2_score_value.grid(row = 0, column = 4)        

    def initGame(self):

        #This starts the game
        #It defines the score variable, binds the keyboard controls
        #It calls createobjects to draw the inital images on the screen
        self.score =  [0, 0]          # added to keep score
        self.count = 0                # to count how many times ball hits paddle
        self.buttons = set()
        self.field_on = False

        self.initUI()
        self.focus_get()
        self.createObjects()
        self.bind_all("<KeyPress>", lambda e: self.buttons.add(e.keysym))
        self.bind_all("<KeyRelease>", lambda e: self.buttons.discard(e.keysym))
        self.after(DELAY, self.onTimer)

    def createObjects(self):

        #This is where I define the game objects based on classes in controller.
        self.padA = Paddle(self, [[550,100], [560, 100], [560, 50], [550, 50]])
        # Made paddle A have a slanted front to see if we could change paddle profile.
        # It works but it allows the paddle to move too low. 
        # The move_line method only use one point to see if the paddle is in the field.
        self.padB = Paddle(self, [[50, 100], [40, 100], [40, 50], [50, 50]])

        self.field = chaotic_field(self, [200, 70, 400, 100], "yellow")

        self.ball = Circle(self, [300,150], [1, -1]) 
        self.ball.draw()

        # Above, Paddle class uses 4 coordinate sets to create instances of Line class


        wall_names = ["lower", "upper", "left", "right"] #These are border lines
        line_points = [[0,300,600,300], [600,0,0,0], [0, 300, 0, 0], [600, 0, 600, 300]]
        args = [(self, 10, item) for item in line_points]
        self.wall_dict = dict(zip(wall_names, [Line(self, item, "white") for item in line_points])) 

        self.bumper = Obstacle(self)
        self.bumper.add_wall([34, 100, 400, 20], "red")

        line4 = Line(self, [300, 175, 300, 225], "green")
        line5 = Line(self, [450, 30, 450, 80], "green")
        self.port = Portal(self, [line4, line5]) # Portal is still buggy.

        line6 = Line(self, [150, 80, 150, 40], "blue")
        line7 = Line(self, [150, 40, 110, 60], "blue")
        line8 = Line(self, [110, 60, 150, 80], "blue") # Adding lines for monster.
        # Monster class is kind of messy and the repeated line instances look cluttered here.
        self.monster = Creature(self, [line6, line7, line8])


        #Might want to reorganize game objects later.
        #Two ways to organize instances of Line class used by other classes
        # One way, the Paddle class creates Line instances within the class.
        # Another way, the Obstacle class takes Line instances as arguments.
        # Not sure how to use inheritance but might make sense to have Paddle and Obstacle inherit Line

    def onTimer(self):

        #This function calls other functions every time step
        #CheckCollision sees if there is a hit
        #doMove moves ball and checks score
        self.doMove()
        self.checkCollision()
        self.after(DELAY, self.onTimer)

    def doMove(self):

        if self.coords(self.ball.image)[0] < self.ball.radius:
            self.score[1] +=1
            self.reset_after_score()
            
        elif self.coords(self.ball.image)[0] > 580-self.ball.radius:
            self.score[0] += 1
            self.reset_after_score()
        else:
            self.ball.new_position(self.ball.velocity)
            if self.field_on == True:
                self.ball.velocity[1] = self.ball.velocity[1] - 0.05

        #Moved keyboard responses of paddles to paddle class method.
        #The move_line method gets called here to move the paddles.
        self.padA.move_line(self.buttons, "Up", "Down")
        self.padB.move_line(self.buttons, "e", "s")
        self.monster.move_creature()

    def checkCollision(self):

        #This uses methods from the Line and Ball class in the controller module.
        #Might want to reorganize to make more concise.
        #Like, somehow combine all the Line class instances to be evaluated in one for loop.
        # Also, when hit_check sees if the ball is above or below the line, it only checks the center of the ball.
        # So if the center of the ball pass just above the paddle, it won't detect a hit even if the radius overlaps the paddle.
        
        x1, y1, x2, y2 = self.bbox(self.ball.image)
        overlap = self.find_overlapping(x1, y1, x2, y2)

        for over in overlap:
            if over == self.field.image:
                self.field.change(self.ball)

        for k,v in self.wall_dict.items():
            if v.hit_check(self.ball):
                self.ball.reflect(v)
            if v.hit_check(self.monster.blob):
                self.monster.blob.reflect(v)

        for item in self.padA.edges + self.padB.edges:
            if item.hit_check(self.ball):
                self.ball.reflect(item)
                self.count += 1
                if self.count == 5:
                    self.count = 0
                    self.ball.velocity += self.ball.velocity

        for item in self.monster.lines:
            if item.hit_check(self.ball, self.monster.blob.velocity):
                self.ball.reflect(item)
                self.monster.stun()

        self.bumper.break_out(self.ball)
        self.port.transport(self.ball)
    
    def reset_after_score(self):
        #This function resets game and updates the score labels.

        self.player1_points.set(self.score[0])
        self.player2_points.set(self.score[1])

        self.ball.position = np.array([300, 150]) # Could use random to make new position/velocity 
        self.ball.velocity = np.array([1, 1])
        self.delete(self.ball.image)
        self.ball.draw()

        self.padB.reset([[50, 100], [40, 100], [40, 50], [50,100]])
        self.padA.reset([[550,50], [560, 100], [560, 50], [550, 50]])

        self.bumper.add_wall([75, 200, 100, 200],"red")
        self.bumper.add_wall([125, 225, 125, 200], "red")


class Protopong(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        parent.title('Protopong')
        self.board = Board(self.parent, self)
        self.initUI()
        self.pack()

    def initUI(self):

        restart = Button(self, text="Restart", command=self.onClick)
        restart.grid(row=1, column = 2)
        quit = Button(self, text = " Quit  ", command=self.quit)
        quit.grid(row=1, column=4)

    def onClick(self):
        self.board.delete(ALL)
        self.board.score =[0,0]
        self.board.initGame()

def main():
    root = Tk()
    prot = Protopong(root)
    root.mainloop()
    root.destroy()
if __name__ == '__main__':
    main()
