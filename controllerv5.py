#Pong Controller

import numpy as np
import random
from numpy.linalg import norm

class Circle:
	def __init__(self, board, pos, vel):
		self.radius = 10
		self.position = np.array(pos, dtype=float)
		self.velocity = np.array(vel, dtype=float)
		self.board = board
		self.image = None

	def draw(self):
		r = self.radius
		x, y = self.position
		self.image = self.board.create_oval(x-r, y-r, x+r, y+r, 
						fill = "white")

	def reflect(self, line, direction=1):
		#Checks for next position directly after collsion and updates velocity vector.
		#For high speeds when the ball moves several pixels per time step,
		#the ball won't always land on the line.
		#Some of the lines are to calculate the balls next position after reflection.

		proj_distance_normal = direction * np.dot(line.positions[0:2] - self.position, line.unormal)*line.unormal
		proj_velocity_normal = direction * np.dot(self.velocity, line.unormal)*line.unormal

		a = norm(proj_distance_normal)-self.radius
		b = norm(proj_velocity_normal)
		scale = a/b
		
		intersection = self.velocity*scale 
		post_hit = self.velocity - intersection
		reflect_post_hit = post_hit - 2*(np.dot(post_hit, line.unormal))*line.unormal
		self.position = intersection + reflect_post_hit + self.position

		#set next position based on where the ball would end up
		#after the time step.
		r = self.radius
		x,y = self.position
		if self.image != None:
			self.board.coords(self.image, (x-r, y-r, x+r, y+r))

		#set new velocity vector.
		self.velocity = self.velocity - 2*proj_velocity_normal

	def new_position(self, next):
		r = self.radius
		x, y = self.position
		self.position = self.position+next
		self.board.coords(self.image, (x-r, y-r, x+r, y+r))


class Line:
	def __init__(self, board, positions, color):
		#This class could be better organized. 
		self.positions = positions

		self.vector = np.array([self.positions[0]-self.positions[2], 
			self.positions[1]-self.positions[3]])
		self.unitv = self.vector/norm(self.vector)
		self.unormal = np.array([-self.unitv[1], self.unitv[0]])
		self.board = board

		self.draw(color)

	def reinit(self):

		self.vector = np.array([self.positions[0]-self.positions[2], 
			self.positions[1]-self.positions[3]])
		self.unitv = self.vector/norm(self.vector)
		self.unormal = np.array([-self.unitv[1], self.unitv[0]])

	def draw(self, color): 
		self.image = self.board.create_line(self.positions, fill=color)

	def hit_check(self, ball, reference = 0):
		self.hit = False

		velocity = ball.velocity - reference
		#Uses projections of ball velocity and lines to find collision.
		#The math works well now. But the code could be more concise.
		#Only issue with the way it works is that it doesn't detect hits
		#that are at the very edge of the paddle.
		#The code only detects a hit when the velocity vector goes through
		#the paddle. Not when the edge of the ball hits the top edge of the paddle.

		v1 = np.array(self.positions[0:2]) - ball.position #Vectors from ball to line ends.
		v2 = np.array(self.positions[2:4]) - ball.position

		proj_v1_v2 = np.dot(v1, v2) 
		proj_v1_velocity = np.dot(v1, velocity)
		proj_v2_v1 = np.dot(v2,v1)
		proj_v2_velocity = np.dot(v2, velocity)

		within_edges = False # See if velocity vector is between vectors from ball to line ends.
		if proj_v1_v2 < proj_v1_velocity and proj_v2_v1 < proj_v2_velocity:
			within_edges = True

		proj_distance_normal = np.dot(v1, self.unormal)
		proj_velocity_normal = np.dot(velocity, self.unormal)
			#See if velocity normal to paddle line exceeds ball distance normal to line.
		
		same_direction = False #See if velocity is towards the line
		if proj_distance_normal*proj_velocity_normal > 0:
			same_direction = True

		if abs(proj_velocity_normal) > abs(proj_distance_normal)-ball.radius:
			if within_edges and same_direction:
				self.hit=True

		return self.hit

class Paddle:

	def __init__(self, board, positions):
		self.board = board
		self.edges = [0,0,0,0]
		for i in range(4):
			coords_set = positions[i-1] + positions[i]
			self.edges[i] = Line( board, coords_set, "white")
		self.speed = 5

	def reset(self, positions):
		for i in range(4):
			coords_set = positions[i-1] + positions[i]
			self.board.delete(self.edges[i].image)
			self.edges[i] = Line(self.board, coords_set, "white")
			self.speed = random.randint(1,10)
	
	def move_line(self, pressed, up, down):

		def change_position(step, lines):
				for line in lines:
					line.positions[1] = line.positions[1]+step
					line.positions[3] = line.positions[3]+step
					self.board.coords(line.image, *line.positions)

		if up in pressed and down not in pressed and self.edges[0].positions[1]>0:
			change_position(-self.speed, self.edges)
			
		elif down in pressed and up not in pressed and self.edges[0].positions[3]<300:
			change_position(self.speed, self.edges)			

class Obstacle():
	#Obstacle made out of lines. 
	#Could make obstacle class inherit line class. 

	def __init__(self, board):
		self.board = board
		self.lines = []

	def add_wall(self, positions, color):
		self.lines.append(Line( self.board, positions, color))


	def break_out(self, ball):
		for line in self.lines:
			if line.hit_check(ball):
				ball.reflect(line)
				line.positions = [0,0,0,0]
				self.board.coords(line.image, *line.positions)

class Portal:

	def __init__(self, board, lines):
		self.lines = lines
		self.board = board

	def transport(self, ball):
		#Changed the arguments for ball.new_position to be based on the unit vector of the ball's velocity.
		#This way, it will move away from the portal after being transported.
		for i in range(len(self.lines)):
			if self.lines[i].hit_check(ball):
				positions1 = np.array(self.lines[i].positions[0:2])
				positions2 = np.array(self.lines[i-1].positions[0:2])
				ball.new_position(positions2-positions1+ball.radius*ball.velocity)

class Creature:
	# Kinda messy but I added a creature class. 
	# Has an issue where the ball reflects but not in the right direction.
	#Uses ball and line classes. The  ball is not drawn. Just use be able to do reflection off borders.
	def __init__(self, board, lines):
		self.lines = lines
		self.board = board
		self.wait = 0
		self.blob = Circle(board, self.lines[0].positions[0:2], [1, 1])
		self.x, self.y = self.blob.velocity


	def move_creature(self):
		self.wait += 1
		self.x, self.y = self.blob.velocity
		for line in self.lines:
			line.reinit()
			line.positions[0] = line.positions[0] + self.x
			line.positions[1] = line.positions[1] + self.y
			line.positions[2] = line.positions[2] + self.x
			line.positions[3] = line.positions[3] + self.y
			self.board.move(line.image, self.x, self.y)
		self.blob.position = np.array(self.lines[0].positions[0:2])
		if self.wait%200 == 0:
			self.x = random.randint(-5,5)
			self.y = random.randint(-5,5)
		self.blob.velocity = np.array([self.x, self.y])

	def stun(self):
		self.wait = 0
		self.blob.velocity = np.array([0,0])

class chaotic_field:
	def __init__(self, board, positions, color):
		self.board = board
		self.positions = positions
		self.image = self.board.create_rectangle(positions, fill=color)	
		self.count = 1

	def change(self, ball):
		self.count += 1

		if self.count%20 == 0:
			x = random.uniform(-3,3)
			y = random.uniform(-3,3)
			ball.velocity = np.array([x, y])
